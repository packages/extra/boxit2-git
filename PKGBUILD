# Maintainter: Bernhard Landauer <bernhard@manjaro.org>

pkgname=boxit2-git
pkgver=r208.f0f9af7
pkgrel=4
pkgdesc="Manjaro Linux repository management client"
arch=('x86_64' 'aarch64')
url="https://gitlab.manjaro.org/tools/maintenance-tools/boxit2"
license=('GPL2')
makedepends=('boost' 'cmake' 'git' 'cxxopts')
depends=('pkg-config' 'grpc' 'protobuf' 'fuse3' 'fmt' 'libsecret' 'openssl'
         'qt5-quickcontrols2' 'qqc2-desktop-style' 'gnome-keyring' 'yaml-cpp')
provides=("${pkgname%-git}")
conflicts=("${pkgname%-git}")
options=('lto')
source=("git+$url.git"
        'https://gitlab.com/LordTermor/boxit2/-/snippets/2088380/raw/#master/server.crt')
sha256sums=('SKIP'
            '6985c731192a640bd214b87c73f38e214febd334b55701e54942e435bba6946f')

pkgver() {
    cd "${pkgname%-git}"
    printf "r%s.%s" "$(git rev-list --count HEAD)" "$(git rev-parse --short HEAD)"
}

build() {

    # prevent static lib mangling with LTO
    CFLAGS+=" -ffat-lto-objects"
    CXXFLAGS+=" -ffat-lto-objects"

    cmake -B build -S "${pkgname%-git}" \
      -DNO_SERVER=1 \
      -DCMAKE_BUILD_TYPE=Debug \
      -DCMAKE_INSTALL_PREFIX="/usr"
      cmake --build build
}

package() {
    DESTDIR="$pkgdir" cmake --install build

    # Configure testing server certificate
    install -Dm644 server.crt "$pkgdir/usr/share/${pkgname%-git}/cert/server.crt"
}
